#include "EncryptionManager.hpp"
#include "Utility.hpp"

char * EncryptionManager::SetupZeroOneBuffer( int maple_key_offset )
{
	char * out_buffer = new char[ 0x78 ]( );

	int i = 0;
	int counter = 0x38;
	int first_index = 0, second_index = 0;
	char first_value = 0, second_value = 0, final_value = 0;

	do
	{
		first_index = ( ( EncryptionManager::SetupBuffer2[ i ] - 1 ) & 7 ) * 4;
		first_value = ( EncryptionManager::SetupBuffer1[ first_index ] );
		second_index = ( EncryptionManager::SetupBuffer2[ i ] - 1 ) >> 3;
		second_value = EncryptionManager::maple_key[ second_index + maple_key_offset ];

		final_value = first_value & second_value;
		out_buffer[ i++ ] = ( final_value != 0 );
	} while ( --counter );


	return out_buffer;
}

void EncryptionManager::StepTwoA( char * buff, char value )
{
	int i = 0;
	do
	{
		char unk = ( i < 0x1C ) ? 0x1C : 0x38;
		auto unk2 = ( value < unk ) ? value : value - 0x1C;
		value++;
		buff[ i + 0x38 ] = buff[ unk2 ];
	} while ( ++i < 0x38 );
}

void EncryptionManager::StepTwoB( char * buff )
{
	int i = 0;
	do
	{
		char val = EncryptionManager::SetupBuffer3[ i + 0x10 ] + 0x37;
		if ( buff[ val ] )
		{
			int index = ( i / 6 ) + 0x70;
			int index2 = ( i % 6 );
			int val2 = ( ( int * )EncryptionManager::SetupBuffer1 )[ index2 ] >> 2;
			buff[ index ] |= val2 & 0xFF;
		}

	} while ( ++i < 0x30 );
}

int * EncryptionManager::StepTwo( char * buff )
{
	char * output = new char[ 0x10 * 4 * 2 ]( ); // loop of 0x10, fill 8 bytes each time.

	int i = 0;
	do
	{
		memset( buff + 0x70, 0, 8 );
		char val = EncryptionManager::SetupBuffer3[ i ];
		StepTwoA( buff, val );
		StepTwoB( buff );
		char one = buff[ 0x70 ];
		char two = buff[ 0x72 ];
		char three = buff[ 0x74 ];
		char four = buff[ 0x76 ];

		output[ i * 8 + 3 ] = one;
		output[ i * 8 + 2 ] = two;
		output[ i * 8 + 1 ] = three;
		output[ i * 8 + 0 ] = four;

		//printf_s( "I is %X\n", i );
		//printf_s( "1. %X %X %X %X\n", one, two, three, four );

		one = buff[ 0x71 ];
		two = buff[ 0x73 ];
		three = buff[ 0x75 ];
		four = buff[ 0x77 ];
		//printf_s( "2. %X %X %X %X\n\n", one, two, three, four );

		output[ i * 8 + 7 ] = one;
		output[ i * 8 + 6 ] = two;
		output[ i * 8 + 5 ] = three;
		output[ i * 8 + 4 ] = four;

	} while ( ++i < 0x10 );

	delete[ ] buff;

	return ( int * )output;
}

void EncryptionManager::StepThree( char * buff )
{
	int i = 0;
	int aids_value = 0x7C;
	do
	{
		int val1 = *( int * )( buff + i );
		int val2 = *( int * )( buff + ( aids_value - 4 ) );
		*( int * )( buff + i ) = val2;
		*( int * )( buff + ( aids_value - 4 ) ) = val1;

		int val4 = *( int * )( buff + i + 4 );
		int val3 = *( int * )( buff + ( aids_value ) );
		*( int * )( buff + i + 4 ) = val3;
		*( int * )( buff + ( aids_value ) ) = val4;

		aids_value -= 0x8;
		i += 8;
	} while ( i < 0x3C );
}

EncryptionManager::EncryptionManager( )
{
	// need to setup our 2 decoding buffers
	auto temp_buffer = this->SetupZeroOneBuffer( );
	this->decoding_buffer1 = this->StepTwo( temp_buffer );
	this->StepThree( ( char * )this->decoding_buffer1 );

	temp_buffer = this->SetupZeroOneBuffer( 8 );
	this->decoding_buffer2 = this->StepTwo( temp_buffer );
	Log( "Decoding buffers at %p and %p", this->decoding_buffer1, this->decoding_buffer2 );
}
#include <intrin.h>
void EncryptionManager::DecryptionRelatedStuff2( int & val_one, int & val_two )
{
	auto uVar1 = _rotl( val_two, 4 );
	val_two = uVar1;
	uVar1 = ( val_one ^ uVar1 ) & 0xf0f0f0f0;
	val_one ^= uVar1;
	uVar1 = val_two ^ uVar1;
	uVar1 = _rotr( uVar1, 0x14 );
	val_two = uVar1;
	uVar1 = ( uVar1 ^ val_one ) & 0xffff0000;
	val_one ^= uVar1;
	uVar1 = val_two ^ uVar1;
	uVar1 = _rotr( uVar1, 0x12 );
	val_two = uVar1;
	uVar1 = ( uVar1 ^ val_one ) & 0x33333333;
	val_one ^= uVar1;
	uVar1 = val_two ^ uVar1;
	uVar1 = _rotr( uVar1, 6 );
	val_two = uVar1;
	uVar1 = ( uVar1 ^ val_one ) & 0xff00ff;
	val_one ^= uVar1;
	uVar1 = val_two ^ uVar1;
	uVar1 = _rotl( uVar1, 9 );
	val_two = uVar1;
	auto uVar2 = ( uVar1 ^ val_one ) & 0xaaaaaaaa;
	uVar1 = val_one ^ uVar2;
	val_one = _rotl( uVar1, 1 );
	val_two ^= uVar2;
}

void EncryptionManager::DecryptionRelatedStuff3( int & val_one, int & val_two )
{
	auto uVar2 = _rotr( val_two, 1 );
	val_two = uVar2;
	auto uVar1 = ( val_one ^ uVar2 ) & 0xaaaaaaaa;
	val_two = uVar2 ^ uVar1;
	uVar1 = val_one ^ uVar1;
	uVar1 = _rotr( uVar1, 9 );
	val_one = uVar1;
	uVar1 = ( uVar1 ^ val_two ) & 0xff00ff;
	val_two = val_two ^ uVar1;
	uVar1 = _rotl( ( val_one ^ uVar1 ), 6 );
	val_one = uVar1;
	uVar1 = ( uVar1 ^ val_two ) & 0x33333333;
	val_two = val_two ^ uVar1;
	uVar1 = val_one ^ uVar1;
	uVar1 = _rotl( uVar1, 0x12 );
	val_one = uVar1;
	uVar1 = ( uVar1 ^ val_two ) & 0xffff0000;
	val_two = val_two ^ uVar1;
	uVar1 = _rotl( val_one ^ uVar1, 0x14 );
	val_one = uVar1;
	uVar1 = ( uVar1 ^ val_two ) & 0xf0f0f0f0;
	val_two = val_two ^ uVar1;
	val_one = _rotr( ( val_one ^ uVar1 ), 4 );
}

int EncryptionManager::GetXorValueFromArrays( int index1, int index2 )
{
	auto one = *( int * )( this->IntArray1 + 4 * ( ( index1 >> 0x18 ) & 0x3F ) );
	auto two = *( int * )( this->IntArray2 + 4 * ( ( index1 >> 0x10 ) & 0x3F ) );
	auto three = *( int * )( this->IntArray3 + 4 * ( ( index1 >> 0x8 ) & 0x3F ) );
	auto four = *( int * )( this->IntArray4 + 4 * ( ( index1 >> 0x0 ) & 0x3F ) );

	auto _one = *( int * )( this->IntArray5 + 4 * ( ( index2 >> 0x18 ) & 0x3F ) );
	auto _two = *( int * )( this->IntArray6 + 4 * ( ( index2 >> 0x10 ) & 0x3F ) );
	auto _three = *( int * )( this->IntArray7 + 4 * ( ( index2 >> 0x8 ) & 0x3F ) );
	auto _four = *( int * )( this->IntArray8 + 4 * ( ( index2 >> 0x0 ) & 0x3F ) );

	auto xor_value = one ^ two ^ three ^ four ^ _one ^ _two ^ _three ^ _four;
	return xor_value;
}

void EncryptionManager::DecryptWithIntArray( int * buffer, int & val_one, int & val_two )
{
	bool second_key = true;
	//Log( "Key one and two are [%X and %X]\n", val_one, val_two );
	int next_key = val_two;

	for ( int i = 0; i < 0x20; i += 2 )
	{
		auto read = buffer[ i ];
		auto index1 = _rotr( next_key, 4 ) ^ read;
		auto index2 = buffer[ i + 1 ] ^ next_key;
		auto xor_value = this->GetXorValueFromArrays( index1, index2 );
		//Log( "Xoring with %X\n", xor_value );
		if ( second_key )
			val_one ^= xor_value, next_key = val_one;
		else
			val_two ^= xor_value, next_key = val_two;

		second_key = !second_key;
	}
}

int * EncryptionManager::DecodeBuffer( int data_size )
{
	int * out_buffer = new int[ ( data_size / 4 ) + 2 ]( ); // 0x8000 total data size, we take 8 bytes at a time.

	for ( int i = 0; i < data_size / 4; i += 2 )
	{
		auto key_one = this->encrypted_pids[ i ];
		auto key_two = this->encrypted_pids[ i + 1 ];

		auto val1 = ( ( key_one >> 0x18 ) & 0xFF ) | ( key_one & 0xFF0000 ) >> 8 | ( key_one & 0xFF00 ) << 8 | ( key_one << 0x18 );
		auto val2 = ( ( key_two >> 0x18 ) & 0xFF ) | ( key_two & 0xFF0000 ) >> 8 | ( key_two & 0xFF00 ) << 8 | ( key_two << 0x18 );

		//Log( "Start val are [%X and %X]", val1, val2 );
		this->DecryptionRelatedStuff2( val1, val2 );
		//Log( "1. Values are now are [%X and %X]", val1, val2 );
		this->DecryptWithIntArray( this->decoding_buffer1, val1, val2 );
		//Log( "2. Values are now are [%X and %X]", val1, val2 );
		this->DecryptWithIntArray( this->decoding_buffer2, val2, val1 );
		//Log( "3. Values are now are [%X and %X]", val1, val2 );
		this->DecryptWithIntArray( this->decoding_buffer1, val1, val2 );
		//Log( "4. Values are now are [%X and %X]", val1, val2 );
		this->DecryptionRelatedStuff3( val1, val2 );
		//Log( "5. Values are now are [%X and %X]", val1, val2 );

		auto to_write1 = ( ( val2 >> 0x18 ) & 0xFF ) | ( val2 & 0xFF0000 ) >> 8 | ( val2 & 0xFF00 ) << 8 | ( val2 << 0x18 );
		auto to_write2 = ( ( val1 >> 0x18 ) & 0xFF ) | ( val1 & 0xFF0000 ) >> 8 | ( val1 & 0xFF00 ) << 8 | ( val1 << 0x18 );
		out_buffer[ i ] = to_write1;
		out_buffer[ i + 1 ] = to_write2;
	}

	return out_buffer;
}

void EncryptionManager::SetEncryptedPidsBuffer( const char * buff )
{
	this->encrypted_pids = ( int * )buff;
}