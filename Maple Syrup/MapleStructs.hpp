#pragma once
#define MI(x,y) x##y
#define M(x,y) MI(x,y)
#define PAD(x) private: char M(_asdf,__COUNTER__)[x]; public: 

namespace MapleStructs
{
	enum class UpdateStat : unsigned char
	{
		Health = 0x4,
		Mana = 0x10,
	};

	enum class AttackType : unsigned char
	{
		Melee = 1,
		Magic = 4,
	};

	struct PacketStruct // [ebp-0x24+4]
	{
		void * vtable; // 0
		int some_number; // 4 remained a 2.
		int unk1; // 8
		int unk2; // C
		int buf_len; // 10
		char * buffer; // 14
		int unk3; // 18
	};

	struct SendStruct
	{
		int unk;
		char * buffer;
		int length;
	};

	struct PacketStructIntermediate
	{
		PAD( 0x4 );
		PacketStruct * pPacketStruct;
	};

	struct PacketIntermediateInstance
	{
		void * vtable; // 0
		short unk;
		short unk2;
		int unk3; // 4
		PacketStructIntermediate * pIntermediate; //
		PacketStructIntermediate * pPacketStruct2; // 68
	};

	struct SocketStruct { // [ebp-0x14]
		PAD( 0xC );
		int socket;
		// 0x10
		PAD( 0x48 );
		PacketIntermediateInstance instance; // 58
	};

	struct PacketStructure // not to be used, purely for me.
	{
		short packet_id;
		short sequence_number;
	};

	struct StreamReader // actually, this is maplestory's streamreader!
	{
		int always_zero_it_appears;
		int always_two_it_appears;
		char * packet_buffer;
		int buffer_size;
		char * unknown_buffer;
		int real_data_size; // size - 4 (because 4 bytes of weird shit at top of buffer. Probably checksum?)
		int read_offset; // this starts from 4 ( ignoring the 4 bytes of weird shit )
	};

	struct Pos
	{
		short x;
		short y;

		Pos( ) { this->x = this->y = 0; }
		Pos( short val ) { this->x = this->y = val; }
		Pos( short x, short y ) { this->x = x; this->y = y; }
		bool Valid( ) { return this->x && this->y; }
		bool operator==( const Pos & other ) { return this->x == other.x && this->y == other.y; }

	};

	struct Monster
	{
		int object_id = 0; // unique.
		int monster_id = 0;
		Pos pos;

		Monster( ) { pos = { 0 }; };
		Monster( int obj_id, int mob_id, Pos pos ) { this->object_id = obj_id; this->monster_id = mob_id; this->pos = pos; }
	};

}