#pragma once
#include <ntdll.h>
#include "../hook_lib/hook_lib/hooklib.hpp"
#include <string>

namespace Utility
{
	void SetByte( uintptr_t loc, unsigned char byte );
	uintptr_t GetModuleSize( uintptr_t base );
	std::string GenerateDumpName( int size, const char* name);
	std::string GenerateDumpName( int size, int id );
	std::wstring GetModuleForAddress( uintptr_t addr, int pid = 0 );
}
