#include "Gaslight.hpp"

Gaslight::~Gaslight( )
{
	if ( this->fake_base )
		VirtualFree( ( void * )this->fake_base, 0, MEM_RELEASE );

	if ( this->ngs_section )
		CloseHandle( this->ngs_section );
}

bool Gaslight::Initialize( )
{
	if ( this->initialized )
	{
		LogB( "Already initialized" );
		return false;
	}

	this->real_base = ( uintptr_t )GetModuleHandle( 0 );
	this->module_size = Utility::GetModuleSize( this->real_base );

	this->fake_base = ( uintptr_t )VirtualAlloc( 0, this->module_size, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE );
	if ( this->fake_base == 0 )
	{
		LogB( "Failed to allocate fake module. %dd", GetLastError( ) );
		return false;
	}

	// can i just lol memcpy or do i need to ensure nothings page_noaccess, fucking yolooooooooooooooooo
	__try
	{
		memcpy( ( void * )this->fake_base, ( void * )this->real_base, this->module_size );
	}
	__except ( 1 )
	{
		LogB( "So..... tough shit!" );
		return false;
	}

	// base, base, size
	this->ngs_section = CreateFileMapping( 0, 0, PAGE_READWRITE, 0, sizeof( void * ) + sizeof( void * ) + sizeof( int ), "shenanigans" );
	if ( this->ngs_section == 0 )
	{
		LogB( "Failed to create NGS section mapping" );
		return true;
	}

	auto mapping = MapViewOfFile( this->ngs_section, FILE_MAP_READ | FILE_MAP_WRITE, 0, 0, 0 );
	if ( mapping == 0 )
	{
		LogB( "Cant create mapping!" );
		return true;
	}

	struct NGSStruct
	{
		uintptr_t real;
		uintptr_t fake;
		uintptr_t size;// of both.
	};

	NGSStruct s = { this->real_base, this->fake_base, this->module_size };
	*( NGSStruct * )mapping = s;

	UnmapViewOfFile( mapping );

	return true;
}

bool Gaslight::IsAddressInModule( uintptr_t addr )
{
	return ( addr >= this->real_base && addr <= ( this->real_base + this->module_size ) );
}

uintptr_t Gaslight::SwapAddr( uintptr_t address )
{
	// assuming addr is in module.
	auto scrap = address;
	scrap -= this->real_base;
	scrap += this->fake_base;
	return scrap;
}
