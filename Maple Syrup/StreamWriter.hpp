#pragma once
#include <vector>
#include <string>

class StreamWriter
{
	std::vector<char> packet_buffer;
	int write_offset = 0;

	char * AtCurrentPos( );
	void IncreaseBuffer( int amount );
	bool HasSpace( int type_size );

public:
	StreamWriter( );
	StreamWriter( char * buf, int len );

	int GetCurrentOffset( );
	void SetCurrentOffset( int offset );
	int GetLength( );

	void WriteLongLong( long long towrite );
	void WriteInt( int towrite );
	// if length is 0, Write til null terminator
	void WriteString( const char * towrite, int len = 0 );
	void WriteSignedShort( short towrite );
	void WriteShort( unsigned short towrite );
	void WriteFloat( float towrite );
	void WriteByte( unsigned char towrite );

	void SkipBytes( int count );
	void Trim( ); // remove everything after write_offset.

	template <typename t>
	void WriteType( t & toWrite )
	{
		if ( !this->HasSpace( sizeof( toWrite ) ) )
			this->IncreaseBuffer( sizeof( toWrite ) );

		memcpy( this->AtCurrentPos( ), &toWrite, sizeof( toWrite ) );
		//*( decltype( towrite ) * )this->AtCurrentPos( ) = towrite;
		write_offset += sizeof( toWrite );
	}

	std::vector<char> GetBuffer( );
};