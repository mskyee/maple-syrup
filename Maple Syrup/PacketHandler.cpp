#include "PacketHandler.hpp"
#include "Utility.hpp"
#include <fstream>
#include "EncryptionManager.hpp"
#include <algorithm>
#include "PacketHacks.hpp"

#undef SendMessage

namespace PacketHandler
{
#define FASTDUMP( name ) DumpPacket(buf, len, std::string(name).c_str() )

	MapleStructs::Pos player_pos;
	int unlucky_monster = 0, last_hit_monster = 0;
	std::vector<MapleStructs::Monster> monster_list;

	std::vector<char> HandlePacket( char * unenc_buffer, int len, bool send )
	{
		StreamReader just_for_pid_lol( unenc_buffer, len );
		auto pid = just_for_pid_lol.ReadShort( );

		auto handler = encrypted_handlers.find( pid );
		if ( handler != encrypted_handlers.end( ) ) // check encrypted pids first, as they can overwrite normal ones.
			return handler->second( unenc_buffer, len );
		else
		{
			auto handler = packet_handlers.find( pid );
			if ( handler != packet_handlers.end( ) ) // not encrypted, so normal!
				return handler->second( unenc_buffer, len );
			else
				return UnknownPacketHandler( unenc_buffer, len, send );
		}
	}

	void DumpPacket( char * buf, int len, const char * name )
	{
		auto gen_name = Utility::GenerateDumpName( len, name );
		std::ofstream out_dump( gen_name.c_str( ), std::ios::binary );
		out_dump.write( buf, len );
		out_dump.close( );
	}

	void DumpPacket( char * buf, int len, int pid )
	{
		auto gen_name = Utility::GenerateDumpName( len, pid );
		std::ofstream out_dump( gen_name.c_str( ), std::ios::binary );
		out_dump.write( buf, len );
		out_dump.close( );
	}

	std::vector<char> UnknownPacketHandler( char * buf, int len, bool send )
	{
		StreamReader just_for_pid_lol( buf, len );
		auto pid = just_for_pid_lol.ReadShort( );
#define UNIQUE 1
#if UNIQUE
		static std::vector<int> pids;
		if ( std::find( pids.begin( ), pids.end( ), pid ) == pids.end( ) )
		{
			LogB( "Unseen unique packet id %X, len %d", pid, len );
			pids.push_back( pid );
		}
#endif
		char temp_buf[ 250 ] = { 0 };
		sprintf_s( temp_buf, "%s_ID_%X", send ? "SEND" : "RECV", pid );
		//FASTDUMP( temp_buf );
		return {};
	}

	std::vector<char> UnknownPacketHandler( char * buf, int len )
	{
		StreamReader just_for_pid_lol( buf, len );
		auto pid = just_for_pid_lol.ReadShort( );
		//LogB( "Unknown packet id %X, len %d", pid, len );
		char temp_buf[ 250 ] = { 0 };
		sprintf_s( temp_buf, "TODO_ID_%X", pid );
		//FASTDUMP( temp_buf );
		return {};
	}
#pragma region Send Handlers
	std::vector<char> NoFuckingClue( char * buf, int len )
	{
		FASTDUMP( "POSSIBLY_POS_RELATED" );
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto unk = reader.ReadInt( );
		auto move_type = reader.ReadByte( );
		switch ( move_type )
		{
			case 0xFF:
			{
				int x = 0;
			}
			break;
			case 0x2:
				LogB( "Movement start maybe?" );
				// then its a 0xFF
				// then its 4 bytes of 0s
				break;
			default:
				break;
		}

		return {};
	}

	MapleStructs::Pos ParseMovePlayer( StreamReader & reader, char move_type, std::vector<int> & last_pos_offset )
	{
		bool unseen = false;
		bool has_data = true;
		bool knock_back = false;
		auto read_some_info = [ &reader ]( )
		{
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( ); // 10.

			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( ); // 6.
			return reader.ReadByte( ); // move_type
		};

		MapleStructs::Pos old_pos, pos = { 0 };

		last_pos_offset.push_back( reader.GetCurrentOffset( ) );
		while ( has_data && !unseen )
		{
			switch ( move_type )
			{

				case 0:
				{
					last_pos_offset.push_back( reader.GetCurrentOffset( ) ); // regular move
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[0] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}

					move_type = read_some_info( );
					break;
				}
				case 1:
				{
					reader.DiscardBytes( 9 );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) ); // seems to only be if im jumping!
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					if ( old_pos.Valid( ) )
					{
						LogB( "[1] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					}
					move_type = read_some_info( );
					break;
				}
				case 2:
				{
					reader.DiscardBytes( 0x8 );
					move_type = reader.ReadByte( );
					LogB( "[2] Doing stuff lol" );
					//LogB( "[2] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y ); // 2 is knockback related.
					break;
				}
				case 3:
				case 4:
				case 5:
				{
					//auto pos_offset = reader.ReadByte( ); // seems to always be 0x8.
					knock_back = true;

					//LogB( "[2] Pos offset is %X (%X)", pos_offset, pos_offset & 0xF ); // perhaps unk_test & 0xF is an offset we need to add to get to position.
					//reader.DiscardBytes( pos_offset & 0xF );
					//StreamWriter w( reader.GetBuffer( ), reader.GetBufferLength( ) );
					//w.SetCurrentOffset( reader.GetCurrentOffset( ) );
					//w.WriteLongLong( 0 );
					//reader.DiscardBytes( 9 );
					//auto wbuf = w.GetBuffer( );
					//std::copy( wbuf.begin( ), wbuf.end( ), reader.GetBuffer( ) ); // modify knockback in reader struct.

					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					LogB( "NO KLUE" );
					move_type = read_some_info( );

					break;
				}
				case 0xC:
				{
					//reader.ReadShort( );
					//last_pos_offset = reader.GetCurrentOffset( );
					//old_pos = pos;
					//reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[C] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}
					reader.ReadByte( ); // incrementing counter.
					move_type = reader.ReadByte( );
					//move_type = read_some_info( );
					break;
				}
				case 0xE: // VERIFIED.
				{
					reader.ReadByte( );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[E] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}
					move_type = read_some_info( );
					break;
				}
				case 0x11:
					has_data = false;
					break; // nothing else ehre, literally all 0. // actually, sometimes we got non-zeros.
				case 0x12:
				case 0x14:
				{
					for ( int i = 0; i < 13; i++ )
						auto cur_short = reader.ReadShort( ); // idk why its like this.

					reader.ReadByte( );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[12] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}
					move_type = read_some_info( );

					break;
				}
				case 0x1E:
				{
					// maybe knockback? dont think so.
					reader.DiscardBytes( 0xD );
					move_type = reader.ReadByte( );
					break;
				}
				case 0x2B:
				{
					knock_back = true;
					// knockback maybe?
					auto potential_offset = reader.ReadByte( );
					if ( potential_offset == 6 || potential_offset == 7 )
						reader.DiscardBytes( 0xC );
					else if ( potential_offset == 2 || potential_offset == 3 || potential_offset == 9 )
						reader.DiscardBytes( 3 );

					move_type = reader.ReadByte( );

					LogB( "[2B] Probably a knockback!" );

					break;
				}
				default:
					LogB( "We dont have a clause for this guy! %X", move_type );
					unseen = true;
					//reader.ReadType<decltype( pos )>( pos ); // reading at 0x10
					break;
			}
		}

		if ( unseen )
			DumpPacket( reader.GetBuffer( ), reader.GetBufferLength( ), "SENDMOVE_UNSEEN" );
		else if ( knock_back )
			DumpPacket( reader.GetBuffer( ), reader.GetBufferLength( ), "SENDMOVE_KNOCKBACK" );
		else
			DumpPacket( reader.GetBuffer( ), reader.GetBufferLength( ), "SENDMOVE" );


		return pos;
	}

	std::vector<char> SendMove( char * buf, int len )
	{
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		reader.ReadInt( ); // some kinda character ID, but it doesnt match ours.
		reader.ReadByte( ); // 0x5C in 2 examples. (3 now),
		reader.ReadByte( ); // varied. 0x1F, then 0x1D ( 0xE7 ) at a much further one. Probably a move_id of sorts, but it started at 0x25.. tghen became 0x1F...
		reader.ReadInt( ); // another static number. // might be a short, or a byte.
		reader.ReadInt( ); // 0.

		MapleStructs::Pos oldpos, pos;
		reader.ReadType<decltype( oldpos )>( oldpos ); // reading at 0x10
		reader.ReadInt( ); // all 0. // same as in monster move, probably tells us whether we've moved or not.
		reader.ReadByte( );
		auto move_type = reader.ReadByte( ); // probably that.

		/*int last_pos_offset = 0;*/
		std::vector<int> pos_offsets;
		pos = ParseMovePlayer( reader, move_type, pos_offsets );

		LogB( "PLAYER_MOVEMENT (%d): [ %d x %d ] => [ %d x %d ]", move_type, oldpos.x, oldpos.y, pos.x, pos.y );
		player_pos = pos;

		// think i can just modify the last pos to whatever? xd.
		if ( knockback_hack )
		{
			// if we had knockback then we would have overwritten it in ParseMove.
			// probably have to modify ALL positions in the packet to fully disable the knockback.
			// even then, the client might handle it clientside :/
			//pos.y -= 100;
			//StreamWriter w( buf, len );
			//for( auto & offset : pos_offsets )
			//{
			//	w.SetCurrentOffset( offset );
			//	w.WriteType<MapleStructs::Pos>( pos ); // modify ALL y positions by -100. // Never got kicked for this.
			//}

			//return w.GetBuffer( );
			//return reader.GetVector( );

			//StreamWriter writer( buf, len );
			//writer.SetCurrentOffset( last_pos_offset );
			//MapleStructs::Pos changed = pos;
			//changed.y -= 150; // we dont get kicked, so thats a good start.
			//return writer.GetBuffer( );
		}

		return {};
	}

	MapleStructs::Pos ParseMoveMonster( StreamReader & reader, std::vector<int> & last_pos_offset )
	{
		bool unseen = false;
		auto asdf = reader.GetCurrentOffset( );
		reader.SetCurrentOffset( 2 );
		int obj_id = reader.ReadInt( );
		reader.SetCurrentOffset( asdf );
		auto read_some_info = [ &reader, &obj_id ]( )
		{
			//auto move_speed_maybe = reader.ReadByte( );
			reader.ReadByte( ); // 1
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( ); // 5 (might change to 0x2A on move, 0x2B on static? or 21?)
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( );
			reader.ReadByte( ); // 10.

			reader.ReadByte( );
			reader.ReadByte( );

			/*These describe the action relative to the LAST position we know.*/
			auto direction = reader.ReadByte( ); // 2 = right, 3 = left, 4 = stationary?
			auto unk2 = reader.ReadByte( );
			auto unk3 = reader.ReadByte( ); // 15 
			if ( direction < 4 )
			{
				LogF( "Monster %X moving! (%X %X %X)", obj_id, direction, unk2, unk3 );
			}
			else
			{
				LogF( "Monster %X static. (%X %X %X)", obj_id, direction, unk2, unk3 );
			}

			reader.ReadByte( ); // 6 is the remaining_entries.
			return reader.ReadByte( ); // { remaining_entries, move_type }. & 0xF = movetype, >> 4 & 0xF = remaining entries
		};

		MapleStructs::Pos old_pos, pos = { 0 };
		auto remaining_entries = reader.ReadByte( );
		char move_type = reader.ReadByte( );

		last_pos_offset.push_back( reader.GetCurrentOffset( ) );
		for ( int i = remaining_entries; i > 0; i-- )
		{
			switch ( move_type )
			{
				case 0://regular move
				{
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[0] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}

					move_type = read_some_info( );
					break;
				}
				case 1: // jumping
				{
					reader.DiscardBytes( 9 );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					if ( old_pos.Valid( ) )
					{
						LogB( "[1] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					}
					move_type = read_some_info( );
					break;
				}
				case 2:
				case 3:
				case 4:
				case 5:
				{ // knockback
					//auto pos_offset = reader.ReadByte( ); // seems to always be 0x8.

					//LogB( "[2] Pos offset is %X (%X)", pos_offset, pos_offset & 0xF ); // perhaps unk_test & 0xF is an offset we need to add to get to position.
					//reader.DiscardBytes( pos_offset & 0xF );

					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					if ( old_pos.Valid( ) )
					{
						LogB( "[2] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y ); // 2 is knockback related.
					}
					move_type = read_some_info( );

					break;
				}
				case 0xC:
				{
					reader.ReadByte( ); // incrementing counter.
					move_type = reader.ReadByte( );
					//move_type = read_some_info( );
					break;
				}
				case 0xE: // VERIFIED.
				{
					reader.ReadByte( );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[E] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}
					move_type = read_some_info( );
					break;
				}
				case 0x11:
					break; // nothing else ehre, literally all 0. // actually, sometimes we got non-zeros.
				case 0x12:
				case 0x14:
				{
					for ( int i = 0; i < 13; i++ )
						auto cur_short = reader.ReadShort( ); // idk why its like this.

					reader.ReadByte( );
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );
					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					//if ( player && old_pos.Valid( ) )
					//{
					//	LogB( "[12] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					//}
					move_type = read_some_info( );

					break;
				}
				case 0x2B:
				{
					// knockback maybe?
					auto unk1 = reader.ReadShort( );
					auto unk2 = reader.ReadShort( );
					reader.ReadByte( );
					// every 0x2B has an accompanying [2] right after it though.
					last_pos_offset.push_back( reader.GetCurrentOffset( ) );

					old_pos = pos;
					reader.ReadType<decltype( pos )>( pos );
					if ( old_pos.Valid( ) )
					{
						LogB( "[2B] Moving from [%d,%d] => [%d,%d]", old_pos.x, old_pos.y, pos.x, pos.y );
					}

					move_type = read_some_info( );
					break;
				}
				default:
					//if ( player )
				{
					LogB( "We dont have a clause for this guy! %X", move_type );
				}
				unseen = true;
				//reader.ReadType<decltype( pos )>( pos ); // reading at 0x10
				break;
			}
		}

		if ( unseen )
			DumpPacket( reader.GetBuffer( ), reader.GetBufferLength( ), "SENDMONSTERPOS_UNSEEN" );
		return pos;
	}

	std::vector<char> SendMonsterPos( char * buf, int len )
	{
		StreamReader r( buf, len );
		r.ReadShort( );
		if ( !r.HasData( ) )
			return {}; // sometimes theyre empty, apparently.

		auto object_id = r.ReadInt( );
		auto move_id = r.ReadShort( );
		r.ReadByte( ); // whether the monster is using a skill or not?
		r.ReadByte( ); // what skill its using ( 0xFF if we have no skill/not using a skill )
		r.DiscardBytes( 0x49 ); // now reading at 0x53
		MapleStructs::Pos oldpos;
		auto old_pos_offset = r.GetCurrentOffset( );
		r.ReadType<decltype( oldpos )>( oldpos );
		auto some_num = r.ReadByte( );
		r.DiscardBytes( 3 ); // now reading at 0x5D
		auto remaining_entires_pos = r.GetCurrentOffset( );
		auto entries = r.ReadByte( );
		r.SetCurrentOffset( remaining_entires_pos );
		//if ( some_num )
		//{
		//	LogB( "Monster %X should change start pos", object_id );
		//}
		// the parsemovemonster would tell us that.

		MapleStructs::Pos pos;
		std::vector<int> pos_offsets;
		pos = ParseMoveMonster( r, pos_offsets );

		if ( pos == oldpos )
		{
			LogF( "Monster %X stationary at [%d x %d]", object_id, pos.x, pos.y );
			FASTDUMP( "SENDMONSTERPOS_STATIONARY" );

		}
		else
		{
			LogF( "Monster %X moved from [%d x %d] to [%d x %d]", object_id, oldpos.x, oldpos.y, pos.x, pos.y );
			FASTDUMP( "SENDMONSTERPOS_MOVING" );
		}

		auto f = std::find_if( monster_list.begin( ), monster_list.end( ), [ object_id ]( MapleStructs::Monster mob ) -> bool
		{
			return ( mob.object_id == object_id );
		} );
		if ( f != monster_list.end( ) )
			f->pos = pos;

		// TODO: So the thing is, we update every monsters position ONCE. then we get kicked on the second attempt.

		if ( player_pos.Valid( ) && knockback_hack )
		{
			//LogB( "Overwriting mob position." );
			//pos.x -= rand( ) % 20;
			auto temp = oldpos;
			StreamWriter w( buf, len );
			w.SetCurrentOffset( remaining_entires_pos );
			w.WriteByte( 0 );
			//for ( auto & offset : pos_offsets )
			//{
			//	w.SetCurrentOffset( offset );
			//	temp.x -= rand( ) % 5;
			//	w.WriteType<MapleStructs::Pos>( temp ); // modify ALL y positions by -100. // Never got kicked for this.
			//	temp = oldpos; // so we only jiggle the old pos.
			//	//w.WriteByte( 0 ); // so theyre not moving!
			//	w.SetCurrentOffset( w.GetCurrentOffset( ) + 0xC );
			//	w.WriteByte( 3 ); // movespeed.
			//	w.SkipBytes( 1 );
			//	w.WriteByte( 0 ); // not 4.
			//}
			auto remaining_len = r.GetRemainingLength( );
			auto remainder = r.ReadString( remaining_len );
			w.Trim( );
			return w.GetBuffer( );

			auto last_pos = pos_offsets.back( );
			w.SetCurrentOffset( last_pos + 0x10 + sizeof( MapleStructs::Pos ) );
			w.WriteByte( 0 );
			w.WriteType<MapleStructs::Pos>( oldpos );



			w.WriteInt( 0 );
			w.WriteInt( 0x26 ); // no impact.
			//w.SkipBytes( 4 );
			w.WriteInt( 0 );
			w.WriteByte( 5 ); // cannot be 0 or 1. probably has to be in range 2->5.. All stationary targets were either 4 or 5 though.
			//w.SkipBytes( 1 );
			w.WriteByte( 0x84 ); // no impact changed to 0. (always 0x38 for stationaries)
			w.WriteByte( 4 ); // no impact changed to 0 (instead of 4) (always 4 for stationarys)
			w.WriteShort( 0 );
			w.WriteString( remainder.data( ), remaining_len ); // this changed, of course.

			// what if we just tack on old_pos to the very end?
			// perhaps all this is verified with the server and is just ignored.. But then why have us send it?
			return w.GetBuffer( );
		} // got kicked for this.

		// cant modify y pos by even ONE. Perhaps theres a checksum in the packet that causes it to fail otherwise. idk.
		return {};
	}

	std::vector<char> SendSpendSkillPoint( char * buf, int len )
	{
		FASTDUMP( "SPENDSKILLPT" );
		StreamReader r( buf, len );
		r.ReadShort( );
		r.ReadInt( );
		return {};

	}


	std::vector<char> SendItemPickup( char * buf, int len )
	{
		FASTDUMP( "ITEMPICKUP" );
		StreamReader r( buf, len );
		r.ReadShort( );
		r.ReadByte( );
		r.ReadInt( ); // tick?
		MapleStructs::Pos me;
		r.ReadType<decltype( me )>( me );

		auto obj_id = r.ReadInt( );
		LogB( "Object ID is %X", obj_id
		);
		return {};
	}


	std::vector<char> SendTakeDamage( char * buf, int len )
	{
		FASTDUMP( "SENDTAKEDMG" );
		StreamReader r( buf, len );
		r.ReadShort( );
		r.DiscardBytes( 0xE );

		auto out_buf = PacketHacks::GodMode( r );
		int damage_taken = r.ReadInt( );
		LogB( "Took %d damage, setting to 0", damage_taken );
		return out_buf;
	}

	std::vector<char> SelectWorld( char * buf, int len )
	{
		FASTDUMP( "SELECTWORLD" );
		StreamReader reader( buf, len );
		reader.DiscardBytes( sizeof( short ) ); // who cares id lol.
		auto unkn = reader.ReadByte( );
		reader.DiscardBytes( sizeof( short ) ); // was all 0.
		auto server_id = reader.ReadInt( );
		reader.DiscardBytes( sizeof( char ) );
		return {};
	}

	std::vector<char> SelectChannel( char * buf, int len )
	{
		// does CPU stuff too. some weird thing tbh.

		FASTDUMP( "SELECTCHANNEL" );
		return {};
	}

	std::vector<char> SendPIN( char * buf, int len )
	{
		FASTDUMP( "SENDPIN" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( );
		if ( len == 6 )
		{
			LogB( "Empty SendPIN packet. Dunno what these are." );
			return {};
		}
		reader.DiscardBytes( sizeof( int ) ); // unk;

		StreamWriter writer( buf, len );

		auto some_len = reader.ReadShort( );
		auto string_location = reader.GetCurrentOffset( );
		auto input_pin = reader.ReadString( some_len );
		auto desired_character_id = reader.ReadInt( ); // 0x4F8A8601 for Aurora char for example!
		LogB( "Sending PIN of length %d [%s]", some_len, input_pin.c_str( ) );
		LogB( "Character ID is %X", desired_character_id );
		auto unk = reader.ReadByte( );
		auto mac_len = reader.ReadShort( );
		auto network_mac_addr = reader.ReadString( mac_len ); // contains ALL active MACs, not just current connection.
		auto some_hwid_kinda_thing_len = reader.ReadShort( );
		auto some_hwid_kinda_thing = reader.ReadString( some_hwid_kinda_thing_len );
		if ( some_len != ( sizeof( ms_defines::ms_pin ) - 1 ) )
		{
			LogB( "PIN len is not %d, going to just write the full packet here.", ( sizeof( ms_defines::ms_pin ) - 1 ) );
			return PacketHacks::OverwritePIN( reader, pid, desired_character_id, unk, mac_len, network_mac_addr, some_hwid_kinda_thing_len, some_hwid_kinda_thing );
		}
		else
		{
			writer.SetCurrentOffset( string_location ); // YAY! it works! :)
			const char * valid_pin = ms_defines::ms_pin;
			writer.WriteString( valid_pin, some_len );
			LogB( "Overwrote invalid pin with %s!", valid_pin );
			auto out_len = writer.GetLength( );
			if ( out_len > len )
			{
				LogB( "Writing a greater number of bytes into packet than expected. (Expected: %X vs %X)", len, out_len );
			}
			else
				return writer.GetBuffer( );//	memcpy( buf, writer.GetBuffer( ), writer.GetLength( ) );
		}
		return {};
	}

	std::vector<char> SelectCharacter( char * buf, int len )
	{
		FASTDUMP( "SELECTCHAR" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( );
		auto char_id = reader.ReadInt( );
		auto some_len = reader.ReadShort( );
		auto char_name = reader.ReadString( some_len );
		LogB( "Attempting to login with character %X [%s]", char_id, char_name.c_str( ) );
		unlucky_monster = char_id;
		// rest is unknown.
		return {};
	}

	std::vector<char> SendAttack( char * buf, int len )
	{
		StreamReader reader( buf, len );
		reader.ReadShort( );
		// update

		MapleStructs::AttackType attack_type;
		reader.ReadType<MapleStructs::AttackType>( attack_type ); // actually no. this also works on our Q ability.
		StreamWriter writer( buf, len );

		writer.SetCurrentOffset( reader.GetCurrentOffset( ) );
		auto target = reader.ReadByte( );


		if ( ( target >> 4 ) == 0 )
		{
			FASTDUMP( "SENDATTACK_MISS_MELEE" );
			LogB( "Didnt hit anyone!" );
			//// theoretically, i could modify this to hit somebody, or everybody, and then i'd be able to hit things even when im nowhere near them.
			////LogB( "Small damage packet!" );
			//if ( instagib_enabled && last_hit_monster )
			//{
			//	writer.WriteByte( 0x1F ); // hit 1 target, 15 times.
			//	reader.DiscardBytes( 0x46 );
			//	writer.SetCurrentOffset( reader.GetCurrentOffset( ) );
			//	writer.WriteInt( last_hit_monster );
			//	writer.WriteByte( 7 );
			//	//writer.WriteShort( 0 );
			//	writer.WriteInt( 0 ); // should be ashort, followed b 2 bytes, but idk what those 2 bytes are.
			//	writer.WriteInt( 0 ); // again, unk numbers.
			//	writer.WriteByte( 0 );
			//	writer.WriteType<MapleStructs::Pos>( monster_pos );
			//	writer.WriteType<MapleStructs::Pos>( monster_pos );
			//	writer.WriteShort( 0x11B ); // ? lol.
			//	writer.WriteLongLong( 0 ); // actually zero.
			//	// now first instance of damage!
			//	for ( int i = 0; i < 0xF; i++ )
			//	{
			//		char damage = rand( ) % 0x13;
			//		writer.WriteInt( damage );
			//		writer.WriteInt( 0 );
			//	}
			//	writer.WriteInt( 0 );
			//	writer.WriteInt( 0 );
			//	writer.WriteInt( 0 );
			//	writer.WriteInt( 0 );
			//	writer.WriteInt( 0 );
			//	writer.WriteShort( 0 ); // LOL WE GON CRASH.
			//	auto val = MapleStructs::Pos( player_x, player_y );
			//	writer.WriteType<MapleStructs::Pos>( monster_pos ); // say we're right next to them?? BIG PLAYS???
			//	LogB( "ATTEMPTING TO OVERWRITE MISS LOL" );
			//	return writer.GetBuffer( );
			//}
		}
		else
		{

			// TODO: Perhaps modify targets to > 1, but make them hit the same target, so then i could have 15 * X hits on the SAME target.
			// However, there may be checks if my attack is hitting multiple targets and it shouldn't. (openMS checks this, by seeing if the skill is able to hit multiple.)
			/*
				THIS WORKED! SO WHAT WE MUST DO THEN IS SAY WE HIT MULTIPLE TIMES???
				WORKED TOO ( as in we crashed )
				So what needs to happen is we need to replicate the damage.
				WORKED. I can insta kill now, lMFOA.
			 */

			reader.ReadInt( ); // 0.
			auto damage_type_idk = reader.ReadByte( ); // perhaps? Or is it a full int?? // 0xC for my Q, 0xE for my W.
			if ( damage_type_idk != 0 )
			{
				// looks like on level-up wedamage all enemies in an area with magic as well.
				FASTDUMP( "SENDATTACK_MAGIC" );
				auto unk_doesnt_change = reader.ReadInt( );
				reader.ReadInt( );
				auto spell_id = reader.ReadInt( );
				reader.DiscardBytes( 0x2B );
				// 0x1A for my W, 0x1F for my Q.
				auto spell_id_short = reader.ReadByte( );

				StreamWriter writer( buf, len );
				writer.SetCurrentOffset( 3 ); // now writing TARGET
				if ( instagib_enabled )
					writer.WriteByte( 0x15 ); // 5 hits now. // 15 hits baybeee. (SHould totally ban me but bad game so lets sEEE)

				auto hit_count = target & 0xF;

				reader.DiscardBytes( 0x13 ); // we are now reading at DAMAGE (if we only had 1 target.)
				for ( int i = 0; i < ( ( target >> 4 ) & 0xF ); i++ ) // target count
				{
					auto hit_monster = reader.ReadInt( );
					reader.DiscardBytes( 0xA );
					MapleStructs::Pos oldPos, newPos;
					reader.ReadType<MapleStructs::Pos>( oldPos );
					reader.ReadType<MapleStructs::Pos>( newPos );
					reader.DiscardBytes( 0xA );
					int total_damage = 0;
					int max_damage = 0;

					for ( int j = 0; j < ( target & 0xF ); j++ )
					{
						// hit count
						auto dmg = reader.ReadInt( );
						total_damage += dmg;
						reader.ReadInt( ); // 0
						max_damage = dmg; // lets try last damage?
					}

					//if ( instagib_enabled )
					//{
					//	// insert attacks here.
					//	writer.SetCurrentOffset( reader.GetCurrentOffset( ) ); // writing now at the end of the last attack!
					//	for ( int j = 0; j < 5 - hit_count; j++ )
					//	{
					//		writer.WriteInt( max_damage );
					//		writer.WriteInt( 0 );
					//	}

					//	auto old_pos = reader.GetCurrentOffset( );
					//	auto remainder = reader.GetRemainingLength( );
					//	auto stuff = reader.ReadString( remainder );
					//	reader.SetCurrentOffset( old_pos );
					//	writer.WriteString( stuff.data( ), remainder );
					//}

					LogB( "[DISABLED] MAGIC Damaging monster %X %d times for a total of %d!", hit_monster, ( target & 0xF ), total_damage );
					reader.DiscardBytes( 0x16 ); // get to next hit monster.
				}

				if ( instagib_enabled )
				{
					//LogB( "But instagib is on, so we're sending 15 hits of MAX DAMAGE!" );

					//return writer.GetBuffer( );
				}
			}
			else
			{
				FASTDUMP( "SENDATTACK_MELEE" );

				// update this.
				reader.DiscardBytes( 0x3D );
				auto hit_monster = reader.ReadInt( );
				last_hit_monster = hit_monster;
				auto post_mob = reader.GetCurrentOffset( );
				LogB( "Last hit monster is now %X", last_hit_monster );
				/*
					//writer.WriteInt( 0 ); // NOTE: This worked! So we can DEFINITELY attack enemies that arent even anywhere near us.
					We did eventually get kicked for this though. However, this should mean that setting damage is 100% POSSIBLE.
					No, think that was something else. This time we set it to 0 (says we hit someone, but the object id literally doesnt exist)
					and nothing died. As expected. So we can almost 100% certainly add damage to this packet.
				*/

				reader.DiscardBytes( 0xA );
				MapleStructs::Pos oldPos, newPos;
				reader.ReadType<MapleStructs::Pos>( oldPos );
				reader.ReadType<MapleStructs::Pos>( newPos );
				// now reading at offset 0x5C.
				reader.DiscardBytes( 0xA );

				auto dmg = reader.ReadInt( );
				reader.ReadInt( ); // should be zero.

				/*
					reader.ReadInt( ); // also zero.
				if ( instagib_enabled )
				{
					auto hit_count_and_targets = ( 0x20 | 0xF ) ; // also FIVE targets!
					writer.WriteByte( hit_count_and_targets ); // modifying target.
					writer.SetCurrentOffset( post_mob + 3 ); // setting those 2 bytes to 0 seems to be fine! Awesome.
					writer.WriteShort( 0 ); // overwrite those 2 bytes we have no idea about.

					writer.SetCurrentOffset( reader.GetCurrentOffset( ) );
					// write a second instance of damage here.

					auto remaining_len = reader.GetRemainingLength( );
					auto remainder = reader.ReadString( remaining_len );
					//writer.WriteString( remainder.data( ), remaining_len );

					auto modified_targets = ( hit_count_and_targets >> 4 ) & 0xF;
					auto og_targets = ( target >> 4 ) & 0xF;

					for ( int y = 0; y < ( modified_targets - og_targets ); y++ )
					{
						for ( int i = 0; i < ( hit_count_and_targets & 0xF ) - ( target & 0xF ); i++ ) // - target & 0xF because that contains the amount of hits we legitimately sent.
						{
							writer.WriteInt( 0x20 );
							writer.WriteInt( 0 ); // idk why, but this has to be zero.
						}

						writer.WriteInt( 0 );
						LogB( "Y == %d", y );

						writer.WriteString( remainder.data( ), 0x12 );
						// otherwise, add mob info here.
						auto current_mob = monster_list.at( y );
						writer.WriteInt( current_mob.object_id );
						writer.WriteByte( 7 );
						//writer.WriteShort( 0 );
						writer.WriteInt( 0 ); // should be ashort, followed b 2 bytes, but idk what those 2 bytes are.
						writer.WriteString( unk_but_important_data.data( ), 5 ); // again, unk numbers.
						writer.WriteType<MapleStructs::Pos>( current_mob.pos );
						writer.WriteType<MapleStructs::Pos>( current_mob.pos );
						writer.WriteShort( 0x11B ); // ? lol.
						writer.WriteLongLong( 0 ); // actually zero.

						if ( y == ( modified_targets - og_targets ) - 1 )
						{
							for ( int i = 0; i < ( hit_count_and_targets & 0xF ) - ( target & 0xF ); i++ ) // - target & 0xF because that contains the amount of hits we legitimately sent.
							{
								writer.WriteInt( 0x20 );
								writer.WriteInt( 0 ); // idk why, but this has to be zero.
							}
						}
					}
					writer.WriteString( remainder.data( ), remaining_len ); // full 0x16 rather than 0x12.
				*/

				/* ^^ Above is code that attempted to attack 5 enemies at once across the map. When executed, we'd first get a popup:
					"Illegal ... [dont know the rest, we got d/ced after]". Which probably means to say they have some anticheat part.
					However, i don't think its a good one, as we were able to swap to a random ID before that was MIIILES away. Or maybe they werent and i should check if theyre close? Idk.
					Anyways, part of the packet definitely includes some form of coordinates, i dunno which, mine, the enemies? I dunno. Probably why we get d/ced and/or the illegal error.
					Reverted for now because fook it.
				*/

				if ( instagib_enabled )
				{
					LogB( "Damaging monster %X for %d damage. However we're instagibbing!", hit_monster, dmg );
					return PacketHacks::MeleeInstagib( reader, target );
				}
				else
				{
					LogB( "Damaging monster %X for %d damage (%d total hits)", hit_monster, dmg, target & 0xF );
					return {};
				}
			}
		}
		return {};
	}

	std::vector<char> CharLoad_Hyper( char * buf, int len )
	{
		FASTDUMP( "CHARLOAD_HYPER" );

		StreamReader r( buf, len );
		r.ReadShort( );
		auto lent = r.ReadShort( );
		auto hyper_string = r.ReadString( lent );
		auto int_juan = r.ReadInt( );
		auto int_dos = r.ReadInt( ); // increments after int_juan hits something idk.
		//LogB( "ONE: %d TWO: %d", int_juan, int_dos );
		return {};
	}

	std::vector<char> GPUStuff( char * buf, int len )
	{
		FASTDUMP( "GPUSTUFF" );
		return {};
	}

	std::vector<char> SendMessage( char * buf, int len )
	{
		FASTDUMP( "SENDMSG" );

		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto tick = reader.ReadInt( );
		auto msg_len = reader.ReadShort( );
		auto msg = reader.ReadString( msg_len );
		LogB( "Sending a message of size %d [%s]", msg_len, msg.c_str( ) );
		char unknown = reader.ReadByte( );
		return {};
	}
#pragma endregion

#pragma region Recv Handlers
	std::vector<char> ReceiveServers( char * buf, int len )
	{
		FASTDUMP( "RECVSERVERS" );

		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto server_id = reader.ReadByte( );
		if ( server_id == 0xFF )
		{
			LogB( "Finished receiving servers." );
			return {};
		}

		auto chan_name_len = reader.ReadShort( );
		auto channel_name = reader.ReadString( chan_name_len );
		reader.DiscardBytes( sizeof( char ) + sizeof( short ) + sizeof( char ) ); // individual elements, as found from the handler function.
		auto channel_count = reader.ReadByte( );
		int lowest_load = INT_MAX;
		int lowest_chann = 0;

		for ( int i = 1; i < channel_count; i++ )
		{
			// every server has 20 channels.
			chan_name_len = reader.ReadShort( );
			auto numbered_channel_name = reader.ReadString( chan_name_len );
			auto channel_load = reader.ReadInt( );
			auto channel_server_id = reader.ReadByte( );
			if ( channel_server_id != server_id )
			{
				LogB( "Somehow this channel and server id dont match!" );
				//FASTDUMP( "RECVSERVERS" );
			}
			auto channel_id = reader.ReadByte( );
			auto unk = reader.ReadByte( );

			if ( channel_load < lowest_load )
			{
				lowest_load = channel_load;
				lowest_chann = channel_id;
			}
		}

		auto unk = reader.ReadShort( );
		if ( unk != 0 )
		{
			LogB( "Not zero! Something important is happening here!" );
		}

		LogB( "Lowest load on server %s is channel %d", channel_name.c_str( ), lowest_chann + 1 );

		reader.DiscardBytes( sizeof( long long ) ); // end of the road! :)
		return {};
	}

	std::vector<char> ReceiveMonsterHP( char * buf, int len )
	{
		FASTDUMP( "MONSTERHP" );
		StreamReader r( buf, len );
		r.ReadShort( );
		int id = r.ReadInt( );
		int hp = r.ReadInt( ); // short? idk.
		LogB( "Monster ID %X now has %d%% hp", id, hp );
		if ( hp == 0 )
		{
			if ( id == last_hit_monster )
			{
				last_hit_monster = 0;
				LogB( "We killed our target monster." );
			}

			auto f = std::find_if( monster_list.begin( ), monster_list.end( ), [ id ]( MapleStructs::Monster mob ) -> bool
			{
				return ( mob.object_id == id );
			} );
			if ( f != monster_list.end( ) )
				monster_list.erase( f );

			LogB( "Monster %X died.", id );
		}

		return {};
	}

	std::vector<char> ReceiveMapBounds( char * buf, int len )
	{
		FASTDUMP( "RECVBOUNDS" );
		StreamReader reader( buf, len );
		reader.ReadShort( );
		reader.DiscardBytes( 0xE );
		auto x_pos = reader.ReadSignedShort( );
		auto y_pos = reader.ReadSignedShort( );
		LogB( "BOTTOM_RIGHT: [%d x %d]", x_pos, y_pos );
		return {};
	}

	std::vector<char> ReceiveChatMessage( char * buf, int len )
	{
		FASTDUMP( "RECVCHAT" );
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto character_id = reader.ReadInt( );
		bool is_gm = reader.ReadByte( );
		auto msg_length = reader.ReadShort( );
		auto msg = reader.ReadString( msg_length );
		auto sender_name_length = reader.ReadShort( );
		auto sender_name = reader.ReadString( sender_name_length );
		// more stuff after, a repeat of the message and some other stuff.
		return {};
	}

	std::vector<char> ReceiveItemDropNotification( char * buf, int len )
	{
		FASTDUMP( "ITEMNOTIFICATION" );
		StreamReader r( buf, len );

		r.ReadShort( );
		r.ReadByte( );
		bool grabable = r.ReadByte( );
		auto object_id = r.ReadInt( );
		r.DiscardBytes( 0x5 );
		auto item_id = r.ReadInt( );
		r.ReadInt( );
		auto amount = r.ReadInt( );
		r.DiscardBytes( 0x5 );
		MapleStructs::Pos p;
		r.ReadType<decltype( p )>( p );

		LogB( "Item ID of %X dropped at position [%d x %d] of amount %d with object id %X (grabbable: %d)", item_id, p.x, p.y, amount, object_id, !grabable );
		return {};
	}

	std::vector<char> ReceiveMessage( char * buf, int len )
	{
		FASTDUMP( "RECVMESSAGE" ); // ?? what the fuck even is receivemessage vs receivechatmessage
		return {};
	}

	std::vector<char> ReceiveChannels( char * buf, int len )
	{
		FASTDUMP( "RECVCHANNELS" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( );
		auto selected_world = reader.ReadByte( ); // 00, 01, 11, 12. its weird.
		auto unk = reader.ReadInt( );
		//reader.DiscardBytes( 3 ); // all 0 in my experience.
		return {};
	}

	std::vector<char> ListCharacters( char * buf, int len )
	{
		FASTDUMP( "LISTCHARACTERS" );

		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( ); // 0x7
		reader.DiscardBytes( 2 );
		auto normal_length = reader.ReadShort( ); // literally a string that says "normal"
		reader.DiscardBytes( normal_length );
		normal_length = reader.ReadShort( ); // literally samething.
		reader.DiscardBytes( normal_length );
		reader.DiscardBytes( sizeof( int ) * 2 + sizeof( short ) );
		// current datetime as FILETIME!
		FILETIME cur_time = { 0 };
		reader.ReadType<FILETIME>( cur_time ); // maplestory server time or smth.
		reader.DiscardBytes( sizeof( char ) );
		auto character_count = reader.ReadInt( );
		LogB( "We have %d characters", character_count );
		int * char_ids = new int[ character_count ]( );
		for ( int i = 0; i < character_count; i++ )
			char_ids[ i ] = reader.ReadInt( );

		auto some_count = reader.ReadByte( );
		auto unk = reader.ReadInt( );
		if ( unk != 0 )
		{
			// was zero for me.
		}

		reader.ReadByte( ); // is 1 in Aurora server (abcd123dcba)

		for ( int i = 0; i < character_count; i++ )
		{
			auto read_player_stats = [ &reader ]( )
			{
				auto current_cid = reader.ReadInt( );  // first character id! Wooo!
				reader.DiscardBytes( sizeof( int ) ); // in my 1 character server, this is also cid, in my 3 character server, its 0.
				reader.ReadInt( ); // looks kinda important, like another kinda ID.
				auto character_name = reader.ReadString( 13 ); // always 13 bytes, 0 right-padded.
				LogB( "Character name %s", character_name.c_str( ) );
				LogB( "Character id %X", current_cid );
				bool female = reader.ReadByte( ); // 0 = male
				LogB( "Character is female? %d", female );
				auto skin_color = reader.ReadByte( ); // lol.
				auto face = reader.ReadInt( );
				auto hair = reader.ReadInt( );
				auto pet_count = reader.ReadByte( );
				LogB( "Pet count is %d", pet_count );
				if ( pet_count == ( unsigned char )( -1 ) )
					reader.DiscardBytes( sizeof( short ) );
				else
				{
					LogB( "How do you have a pet lol??? FAILED." );
				}
				LogB( "At offset %X", reader.GetCurrentOffset( ) );

				auto char_level = reader.ReadInt( ); // or is it a byte?
				LogB( "Level is %d", char_level );
				auto job = reader.ReadShort( ); // doesnt quite match 2 demon slayers so idk about this, maybe thats different?
				auto strength = reader.ReadShort( );
				auto dexterity = reader.ReadShort( );
				auto intelligence = reader.ReadShort( );
				auto luck = reader.ReadShort( );
				auto cur_hp = reader.ReadInt( );
				LogB( "HP is %d", cur_hp );
				auto max_hp = reader.ReadInt( );
				auto cur_mp = reader.ReadInt( );
				auto max_mp = reader.ReadInt( );
				auto remaining_ap = reader.ReadShort( );
				reader.DiscardBytes( sizeof( short ) );
				auto remaining_sp = reader.ReadShort( );
				LogB( "Remaining AP is %d", remaining_ap );
				LogB( "Remaining SP is %d", remaining_sp );
				reader.DiscardBytes( sizeof( short ) );
				auto current_experience = reader.ReadInt( ); // is this 8 bytes? holy SHITE.
				LogB( "Current EXP is %d", current_experience );
				auto current_fame = reader.ReadInt( ); // idk if thats what this is. Its 0 for me.
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadByte( ); // reading at offset 0x95 in aurora.
				reader.ReadInt( );
				reader.ReadShort( );
				reader.ReadInt( );
				reader.ReadShort( ); // reading at offset 0xA0
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.DiscardBytes( 0x15 ); // unknown type. Reading at offset 0xBE
				reader.ReadInt( );
				reader.ReadByte( );
				reader.ReadInt( );
				reader.ReadByte( );
				reader.ReadByte( );
				reader.ReadInt( ); // reading at offset 0xDE
				reader.DiscardBytes( 27 * sizeof( int ) ); // yeah. they read 27 bytes. 3 * 9
				reader.ReadInt( ); // reading at offset 0x14E
				reader.ReadInt( ); // 0x01D5A664 for reference in aurora.
				// 8 + 8 + (int*5) + char
				constexpr int full_size = 8 + 8 + sizeof( int ) * 5 + sizeof( char ); // 0x25 (37) bytes
				reader.DiscardBytes( full_size ); // some character look or equipment shat.
				reader.ReadByte( );
			};
			read_player_stats( ); // also contains stuff like location but idk where
			// reading now at 0x17C in aurora.
			reader.ReadInt( );
			reader.ReadLongLong( );

			// now reading at 0x188.
			auto read_player_look = [ &reader ]( )
			{
				bool female = reader.ReadByte( );
				auto skin = reader.ReadByte( );
				reader.ReadInt( );
				auto face = reader.ReadInt( ); // 0x18E
				reader.ReadByte( ); //even discarded by the game lol.
				auto body_part = reader.ReadByte( ); // 0x192
				while ( body_part != 0xFF )
				{
					auto style = reader.ReadInt( );
					body_part = reader.ReadByte( );
					// finishes at  0x1B4
				}

				body_part = reader.ReadByte( );
				while ( body_part != 0xFF )
				{
					auto style = reader.ReadInt( );
					body_part = reader.ReadByte( );
					// finishes at  0x1B4
				}

				body_part = reader.ReadByte( );
				while ( body_part != 0xFF )
				{
					auto style = reader.ReadInt( );
					body_part = reader.ReadByte( );
					// finishes at  0x1B4
				}

				body_part = reader.ReadByte( );
				while ( body_part != 0xFF )
				{
					auto style = reader.ReadInt( );
					body_part = reader.ReadByte( );
					// finishes at  0x1B4
				}

				reader.ReadInt( ); // at 0x1B8
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadInt( );
				reader.ReadByte( ); // bool at 0x1CC
				reader.DiscardBytes( 0xC );
				reader.ReadInt( ); // at 0x1D9 // this has a few possibilities that i cant uss out, this is prob right
				//reader.ReadByte( ); // 1DD
				//reader.ReadByte( );
			};
			read_player_look( );
			// now reading at 0x1DF

			/*CHARACTER STATS*/
			//break; // lol idk anymore.

			/*CHARACTER LOOK*/
			// contains equipment stuff.
			// everything from here on is equipment (weapon ids, armor ids, etc.)
		}

		// ooooh boy we're DOIN IT
		// from here on its quite variable, on my server with 1 character, 0x16 bytes, on one with 3, 0x1E bytes until we hit name.
		// need more data.
		return {};
	}

	std::vector<char> ListEvents( char * buf, int len )
	{
		FASTDUMP( "LISTEVENTS" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( ); // 0x132
		reader.DiscardBytes( sizeof( int ) + 1 );
		auto s_len = reader.ReadShort( );
		auto maple_events_string = reader.ReadString( s_len );
		reader.DiscardBytes( 1 + sizeof( int ) );
		reader.DiscardBytes( 1 + 7 ); // 0xA ( newline )
		s_len = reader.ReadShort( );
		auto cur_event = reader.ReadString( s_len );
		reader.DiscardBytes( 1 + sizeof( int ) );
		return {};
	}

	std::vector<char> CharacterLogin( char * buf, int len )
	{
		FASTDUMP( "CHARLOGIN" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( ); // 0x21b
		// NOTE: VERY B1G PACKY.
		return {};
	}

	std::vector<char> SpawnCharacter( char * buf, int len )
	{
		FASTDUMP( "SPAWNCHAR" );
		StreamReader r( buf, len );
		auto pid = r.ReadShort( );
		auto cid = r.ReadInt( );
		auto level = r.ReadInt( );
		auto name_len = r.ReadShort( );
		auto name = r.ReadString( name_len );
		// big packet. most of it is probably char looks tuff.

		return {};
	}

	std::vector<char> SpawnMonster( char * buf, int len )
	{
		FASTDUMP( "SPAWNMOB" );
		StreamReader r( buf, len );
		r.ReadShort( );
		bool controlling = r.ReadByte( );
		auto object_id = r.ReadInt( );
		bool unk = r.ReadByte( ); // wether we dictate where this monster is.
		auto monster_id = r.ReadInt( ); // or is monster ID at 0x8 ?
		r.DiscardBytes( 0xD3 );
		MapleStructs::Pos pos;
		r.ReadType<decltype( pos )>( pos );

		//if ( unlucky_monster == 0 )
		//{
		//	unlucky_monster = perhaps_unique_id;
		//	LogB( "Our unlucky monster is %X", unlucky_monster );
		//}

		LogB( "Monster Object ID %X at [%d x %d]. Control: %d", object_id, pos.x, pos.y, controlling );
		MapleStructs::Monster mob;
		mob.monster_id = monster_id;
		mob.object_id = object_id;
		mob.pos = pos;

		monster_list.push_back( mob );

		return {};
	}

	std::vector<char> UpdateStat( char * buf, int len )
	{
		FASTDUMP( "UPDATESTAT" );
		StreamReader r( buf, len );
		r.ReadShort( );
		r.DiscardBytes( 0x3 );
		MapleStructs::UpdateStat desired_stat = {};
		r.ReadType<decltype( desired_stat )>( desired_stat );
		r.DiscardBytes( 6 );
		int new_stat = 0;

		switch ( desired_stat )
		{
			case MapleStructs::UpdateStat::Health:
				new_stat = r.ReadInt( );
				LogB( "New health is %d", new_stat );
				break;

			case MapleStructs::UpdateStat::Mana:
				new_stat = r.ReadInt( );
				LogB( "New mana is %d", new_stat );
				break;
			default:
				//LogB( "Unknown stat %X. Value: %d", desired_stat, new_stat );
				break;
		}
		return {};
	}

	std::vector<char> SomeHWIDStuff( char * buf, int len )
	{
		FASTDUMP( "HWIDSTUFF" );
		StreamReader reader( buf, len );
		auto pid = reader.ReadShort( );
		auto unk = reader.ReadLongLong( );
		auto lent = reader.ReadShort( );
		auto some_hwid = reader.ReadString( lent );
		LogB( "'HWID' is %s", some_hwid.c_str( ) );
		return {};
	}

	std::vector<char> SelectCharacterResponse( char * buf, int len )
	{
		FASTDUMP( "SELECTCHAR_RESPONSE" );
		return {};
	}

	std::vector<char> MoveMonsterResponse( char * buf, int len )
	{
		FASTDUMP( "MOVEMONSTER_RESPONSE" );
		StreamReader reader( buf, len );
		reader.ReadShort( );
		int mob_id = reader.ReadInt( );
		reader.ReadByte( ); // always zero?
		auto move_id = reader.ReadShort( ); // counter, i++ each time, monster-specific ( monster 0 different counter than monster 1 )
		reader.ReadShort( ); // zero?
		reader.ReadShort( ); // either 0x28, or 0. // ALLEGEDLY: Current mana, according to OpenMS.
		// rest of the packet is 0.

		return std::vector<char>( );
	}

	std::vector<char> InvalidPINResponse( char * buf, int len )
	{
		FASTDUMP( "FAILED_PIN" );

		StreamReader r( buf, len );
		r.ReadShort( );
		auto unk = r.ReadByte( );
		LogB( "Your pin failed, dingus!" );
		return {};
	}

	EncryptionManager * pEncryptionManager = nullptr;
	std::map<short, handler> encrypted_handlers = {};

	std::vector<char> UpdateEncryptedPacketIDs( char * buf, int len )
	{
		FASTDUMP( "ENCRYPTED_PIDS" );
		StreamReader r( buf, len );
		r.ReadShort( );
		r.ReadInt( ); // 4 in all my packets
		auto data_size = r.ReadInt( ) - 8; // 0x00008000 in all packets. 
		auto encoded_buffer = new char[ data_size ]( );
		memcpy( encoded_buffer, r.ReadString( data_size ).data( ), data_size );

		if ( pEncryptionManager == 0 )
			pEncryptionManager = new EncryptionManager( );

		pEncryptionManager->SetEncryptedPidsBuffer( encoded_buffer );
		auto decrypted_pids = pEncryptionManager->DecodeBuffer( data_size );

		DumpPacket( ( char * )decrypted_pids, data_size, "DECRYPTED_PIDS" );

		auto string_to_int = [ decrypted_pids ]( int string_integer )
		{
			char temp[ 5 ] = { 0 };
			*( int * )temp = string_integer;
			return atoi( temp );
		};

		PacketHandler::encrypted_handlers.clear( );
		auto send_attack = string_to_int( decrypted_pids[ 0x4C / 4 ] ); // offset 0x50
		//auto send_ranged_attack = string_to_int( decrypted_pids[ 0x54 / 4 ] ); // slightly different. plus they use ammo so :(
		//auto chat_id = string_to_int( decrypted_pids[ 0x1C ] ); // offset 0x70
		//auto hyper_stuff = string_to_int( decrypted_pids[ 0xBA ] ); // offset 2E8


		// this one matches unencrypted id 0x43D. -- This could still be position, but absolute rather than a velocity
		//auto maybe_pos = string_to_int( decrypted_pids[ 0x371 ] ); // offset 0xDC4 // not that one.
		// this one matches unencrypted id 0x0DB. -- THIS IS DEFINITELY LIKE VELOCITY. ONLY SENT WHEN WE'RE MOVING/JUMPING
		//auto movement_id = string_to_int( decrypted_pids[ 0xF ] ); // offset 0x3C // not that one.
		//auto send_monster_pos = string_to_int( decrypted_pids[ 0x34C ] );// offset 0xD30
		auto take_damage = string_to_int( decrypted_pids[ 0x68 / 4 ] ); // send damage at offset 0x6C, OR 0x15b, but probably 6c.
		auto pickup_item = string_to_int( decrypted_pids[ 0xDB8 / 4 ] );
		//auto update_stat_points = string_to_int( decrypted_pids[ 0x20C / 4 ] );

		// packet 0x429 (unenc) apparently is a short, then monster id,.
		// this appears to be sent when a monster is to die?? IDK.!

		// TODO: Note, sometimes these can overwrite OTHER packet ids ( once had SendMove stuff overwrite SendMove, so thats kinda crazy. )
		PacketHandler::encrypted_handlers.insert( { send_attack, SendAttack } );
		//PacketHandler::encrypted_handlers.insert( { chat_id, SendMessage } );
		//PacketHandler::encrypted_handlers.insert( { hyper_stuff, CharLoad_Hyper } );
		//PacketHandler::encrypted_handlers.insert( { maybe_pos, NoFuckingClue } ); // orperhaps packets that range from 0xC to 0x38 in size??
		//PacketHandler::encrypted_handlers.insert( { movement_id, SendMove } );
		//PacketHandler::encrypted_handlers.insert( { send_monster_pos, SendMonsterPos } );
		PacketHandler::encrypted_handlers.insert( { take_damage, SendTakeDamage } );
		PacketHandler::encrypted_handlers.insert( { pickup_item, SendItemPickup } );
		//PacketHandler::encrypted_handlers.insert( { update_stat_points, SendSpendSkillPoint } );

		LogB( "Encrypted handlers updated." );

		delete[ ] decrypted_pids; // mem leak lol
		return {};
	}
#pragma endregion

#pragma region Heartbeats

	std::vector<char> HeartBeat_6Bytes( char * buf, int len )
	{
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto incremented_counter = reader.ReadShort( );
		auto unk = reader.ReadShort( );
		return {};
	}

	std::vector<char> HeartBeat_8Bytes( char * buf, int len )
	{
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto unk = reader.ReadShort( );
		auto unk2 = reader.ReadShort( );
		auto unk3 = reader.ReadShort( );
		return {};
	}

	std::vector<char> HeartBeat_2Bytes( char * buf, int len )
	{
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( ); // thats it lol.
		return {};
	}

	std::vector<char> HeartBeat_3Bytes( char * buf, int len )
	{
		StreamReader reader( buf, len );
		auto id = reader.ReadShort( );
		auto unk = reader.ReadByte( );
		return {};
	}


#pragma endregion
}