#pragma once
#include <vector>
#include "StreamReader.hpp" 
#include "StreamWriter.hpp"
#include "global_defines.hpp"

namespace PacketHacks
{
	using cheat_ret = std::vector<char>;

	cheat_ret MeleeInstagib( StreamReader & reader, char target );
	cheat_ret GodMode( StreamReader & reader );
	cheat_ret OverwritePIN( StreamReader & reader, short pid, int cid, char unk, short mac_len, std::string mac, short hwid_len, std::string hwid );
}