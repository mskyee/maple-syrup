#include "PacketHacks.hpp"
#include "Utility.hpp"

namespace PacketHacks
{
	cheat_ret MeleeInstagib( StreamReader & reader, char target )
	{
		StreamWriter writer( reader.GetBuffer( ), reader.GetBufferLength( ) );
		writer.SetCurrentOffset( 3 ); // the target thingy, duh.
		auto hit_count_and_targets = ( target | 0xF ); // also FIVE targets!
		writer.WriteByte( hit_count_and_targets ); // modifying target.
		//writer.SetCurrentOffset( post_mob + 3 ); // setting those 2 bytes to 0 seems to be fine! Awesome.
		//writer.WriteShort( 0 ); // overwrite those 2 bytes we have no idea about.

		writer.SetCurrentOffset( reader.GetCurrentOffset( ) );
		// write a second instance of damage here.
		for ( int i = 0; i < ( hit_count_and_targets & 0xF ) - ( target & 0xF ); i++ ) // - target & 0xF because that contains the amount of hits we legitimately sent.
		{
			writer.WriteInt( 0x20 );
			writer.WriteInt( 0 );
		}

		auto remaining_len = reader.GetRemainingLength( );
		auto remainder = reader.ReadString( remaining_len );
		writer.WriteString( remainder.data( ), remaining_len );

		return writer.GetBuffer( );
	}

	cheat_ret GodMode( StreamReader & reader )
	{
		/*
			StreamWriter w( buf, len );
			w.SetCurrentOffset( off );
			w.WriteInt( 0xFFFFFF );
			LogB("Took %d damage, setting to MAX_INT (4294967295)", damage_taken )
			memcpy( buf, w.GetBuffer( ), w.GetLength( ) );
		*/

		// LOOOOOOOOOL i can set damage to 0 WHAT THE FUCK.

		StreamWriter w( reader.GetBuffer( ), reader.GetBufferLength( ) );
		w.SetCurrentOffset( reader.GetCurrentOffset( ) );
		w.WriteInt( 0 );
		return w.GetBuffer( );
	}

	cheat_ret OverwritePIN( StreamReader & reader, short pid, int cid, char unk, short mac_len, std::string mac, short hwid_len, std::string hwid )
	{
		StreamWriter writer( reader.GetBuffer( ), reader.GetBufferLength( ) );

		writer.WriteShort( pid );
		writer.WriteInt( 0 );
		writer.WriteShort( sizeof( ms_defines::ms_pin ) - 1 );
		writer.WriteString( ms_defines::ms_pin, sizeof( ms_defines::ms_pin ) - 1 );
		writer.WriteInt( cid );
		writer.WriteByte( unk ); // 0
		writer.WriteShort( mac_len );
		writer.WriteString( mac.c_str( ), mac_len );
		writer.WriteShort( hwid_len );
		writer.WriteString( hwid.c_str( ), hwid_len );

		return writer.GetBuffer( );
	}
}