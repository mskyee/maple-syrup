#include "Gaslight.hpp"
#include "PacketHandler.hpp"
Utility::HookManager * pHookManager = 0;
#include <DbgHelp.h>
#include "global_defines.hpp"
#pragma comment(lib, "dbghelp")

bool PacketHandler::instagib_enabled = false, PacketHandler::knockback_hack = false;

DWORD __stdcall InputLoop( void * ths )
{
	while ( true )
	{
		if ( GetAsyncKeyState( VK_INSERT ) & 1 )
		{
			Log( "TERMINATING" );
			delete pHookManager;
			delete pGaslight;
			FreeLibraryAndExitThread( ( HMODULE )ths, 0 );
		}
		else if ( GetAsyncKeyState( VK_HOME ) & 1 )
		{
			PacketHandler::instagib_enabled = !PacketHandler::instagib_enabled;
			LogB( "Instagib set to %d", PacketHandler::instagib_enabled );
		}
		else if ( GetAsyncKeyState( VK_PRIOR ) & 1 ) // page up
		{
			PacketHandler::knockback_hack = !PacketHandler::knockback_hack;
			LogB( "Knockback Hack set to %d", PacketHandler::knockback_hack );
		}

		Sleep( 1 );
	}
}

void DecryptHook( Utility::x86Registers * pRegs )
{
	auto pStruct = ( MapleStructs::StreamReader * )( pRegs->ebp - 0x48 );
	auto real_data = pStruct->packet_buffer + 4;

	//LogB( "Recv called! Packet struct at %p, buffer length is %d (decrypted size %d)", pStruct, pStruct->packet_size, pStruct->real_data_size );
	auto perhaps_size = *( int * )( pStruct->packet_buffer - 4 );
	//LogB( "Size %X vs %X", perhaps_size, pStruct->real_data_size );

	auto ret = PacketHandler::HandlePacket( real_data, pStruct->real_data_size, false );
	//if ( pid == 0x2C )
	//{
		//LogB( "Nuking set encrypted structure packet" ); // THATS IT!
		//memset( real_data, 0, pStruct->real_data_size );
	//}

	// here, if we were to overwrite the received packet (idk why that'd be useful), we'd just memcpy into real_data.
}

unsigned short GetEncryptionKey( uintptr_t old_ebp )
{
	return *( unsigned short * )( old_ebp + 0xC );
}

void HowToEncryptPackets( short some_xor_key, short * some_other_xor_key )
{
	auto xor_key = some_other_xor_key[ 1 ];
	auto xor_val = some_xor_key ^ xor_key;
	char * packet_buffer = ( char * )5;
	*( WORD * )packet_buffer = xor_val;
	*( ( WORD * )packet_buffer + 1 ) = ( 0xDEADBEEF ^ xor_val ); // 0xDEADBEEF comes from v6+8, which is the same structure (v6+4) which contained unencrypted data.

	// Then, theoretically, we can just call SEND with the right socket.
	// so... xor_key... since we put it IN the packet buffer, does that mean we can just dictate it to be whatever we want??
}

//0x00E81DB0 (maplestory.exe+A81DB0)

void EncryptionHook( Utility::x86Registers * pRegs )
{
	//auto dest = ( char * )( ( void ** )pRegs->ebp )[ 2 ];
	//auto pStruct = ( MapleStructs::PacketStruct * )pRegs->ebx;
	//auto src = ( char * )( ( void ** )pRegs->ebp )[ 3 ];
	//auto len = ( int )( ( void ** )pRegs->ebp )[ 4 ];
	auto pUnencStruct = ( MapleStructs::SendStruct * )pRegs->edi;
	auto len = pUnencStruct->length;
	auto src = pUnencStruct->buffer;
	static void * big_packet = nullptr;

	if ( big_packet == nullptr )
		big_packet = new char[ USN_PAGE_SIZE ]( ); // allocate 1kb page for use as big packet.

	std::vector<char> full_packet;
	full_packet.assign( src, src + len );
	auto out_packet = PacketHandler::HandlePacket( full_packet.data( ), full_packet.size( ), true );
	//todo: memory free bad
	if ( out_packet.size( ) <= 0 )
		return;

	if ( out_packet.size( ) > len )
	{
		LogF( "Attempting to overwrite packet!" );
		pUnencStruct->length = out_packet.size( ); // sometimes this modification causes a freeze, idk why.
		// unallocate and reallocate buffer ?
		//if ( !HeapFree( heap, 0, src ) )
		//{
		//	LogB( "Failed to free heap! Going to continue anyways tho lololol" );
		//}

		memset( big_packet, 0, USN_PAGE_SIZE );

		memcpy( big_packet, out_packet.data( ), out_packet.size( ) );
		pUnencStruct->buffer = ( char * )big_packet;
		LogF( "Packet should now be successfully overwritten." );
		PacketHandler::DumpPacket( out_packet.data( ), out_packet.size( ), "MODIFIED" );
	}
	else
	{
		// <= len, so no resize needed. (probably == )
		LogF( "Fast overwrite!" );
		memset( src, 0, len );
		memcpy( src, out_packet.data( ), out_packet.size( ) );
		pUnencStruct->length = out_packet.size( );
		PacketHandler::DumpPacket( out_packet.data( ), out_packet.size( ), "FAST_MODIFIED" );
	}

	// TODO: If i wanted to send my own packet rather than rewrite one, we would first, need to have the packet ID get encrypted into that 4 byte fucked shit\
	and 2, need to call the Encryption function with the correct encryption key, is this always the same, sent by the server? Will check.
}

//0x00AF2160 (maplestory.exe+6F2160)
//void EncSpotTwo( Utility::x86Registers * pRegs ) // this one not called at all before login.
//{
//	auto dest = ( char * )( ( void ** )pRegs->ebp )[ 2 ];
//	auto src = ( char * )( ( void ** )pRegs->ebp )[ 3 ];
//	auto len = ( int )( ( void ** )pRegs->ebp )[ 4 ];
//
//	LogB( "DEST BUF: %p, SRC BUF: %p, LEN: %d", dest, src, len );
//	NtSuspendProcess( ( HANDLE )-1 );
//}

void checksum_hook( Utility::x86Registers * pRegs ) // this one not called at all before login.
{
	auto loc = pRegs->ebx;
	if ( pGaslight->IsAddressInModule( loc ) )
	{
		//LogB( "CHECKING LOCATION %p", loc );
		// this is called for literally every single byte in the module until the very end. holy shiiiiit.
		auto fake_addr = pGaslight->SwapAddr( loc );
		pRegs->ebx = fake_addr;
	}
}

void checksum_two_hook( Utility::x86Registers * pRegs )
{
	auto loc = pRegs->edi;
	if ( pGaslight->IsAddressInModule( loc ) )
	{
		auto fake_addr = pGaslight->SwapAddr( loc );
		//LogB( "CHECKING MODULE LOCATION %p. Swapped for %p", loc, fake_addr );
		// can i just do.. this?
		pRegs->edi = fake_addr;
	}
}

Gaslight * pGaslight = nullptr;

void EncryptedPacketIDHook( Utility::x86Registers * pRegs )
{
	//auto val = pRegs->ebx;
	//LogB( "Saw value %X", val );
	struct IDK
	{

		PAD( 0x10 );
		int real_packet_id; // 0x10
		PAD( 0x4 ); // 0x14

		struct IDK2
		{
			short unknown;
		};
		IDK2 * pIDK; // 0x18
	};
	IDK * pStruct = ( IDK * )pRegs->eax;
	//LogB( "Struct at %p", pStruct );
	//LogB( "Points to %p", pStruct->pIDK ); // these are packet id SPECIFIC.
	//LogB( "EncValue is %X", pStruct->pIDK->unknown );
	//LogB( "DecValue is %X", pStruct->real_packet_id );

	if ( PacketHandler::encrypted_handlers.find( pStruct->pIDK->unknown ) == PacketHandler::encrypted_handlers.end( ) )
	{
		LogB( "Saw unknown encrypted packet %X which should be %X", pStruct->pIDK->unknown, pStruct->real_packet_id );
	}
}

void TestHook( Utility::x86Registers * );

/* 
	Dll 入口，本项目最终效果是被编译成 dll 供外界注入或使用
 */
BOOL WINAPI DllMain( HMODULE hThis, DWORD dw, LPVOID )
{
	// DLL 接收到的信息是进行正在挂载 
	if ( dw == DLL_PROCESS_ATTACH )
	{
		// 以当前时间作为随机数种子
		srand( time( 0 ) ); // for stuff used in hecks.
		// 申请调试用控制台，不明觉厉
		AllocConsole( );
		
		FILE * pFile = 0;
		freopen_s( &pFile, "CONOUT$", "w", stdout );

		// 创建 hook 管理器
		pHookManager = new Utility::HookManager( );

		// 创建 Gaslight
		pGaslight = new Gaslight( );
		if ( pGaslight->Initialize( ) == false )
		{
			LogB( "Failed to gaslight! No, you failed to gaslight!" );
			return FALSE;
		}

		// hook send for now.
		// 
		uintptr_t loc = 0;

		/*
			GetModuleHandle 的用途是可以传入一个 string，这个 string 代表某个被程序加载的 dll 的名字
			调用成功后将返回指定 dll 的句柄

			若这个函数被传入 NULL 时，将返回使用当前 dll 的进程的句柄

			那么为什么这里写的是 0 呢？ 0 代表什么？
			在大多数 C/C++ 实现中， NULL 都是是一个宏定义， 被定义为了数值 0
			因此此处写 0 等同于写 NULL，写 NULL 也等同于写 0
		*/
		// 获取使用 dll 的进程的句柄，并将其当作进程起始地址使用
		auto base = ( uintptr_t )GetModuleHandle( 0 );
		
		// 获取进程的内存空间大小
		auto size = Utility::GetModuleSize( base );

		//loc = Utility::FindPattern( base, size, ( char * )"55 8b ec 81 ec 44 01" ); // fun fact, there is an identical function, dunno what thats about.
		//if ( !loc )
		//{
		//	LogB( "Cant find Encryption Function!" );
		//	return FALSE;
		//} // ah. i hook here because trying to hook the OG function has a LOT of pattern matches.

		/*
			根据特征码查找封包函数

			第一个参数是我们之前取到的进程起始地址
			第二个参数代表进程内存空间大小

			结合这里函数的用途，可以进一步得出结论，这套源码编译出的 dll 是要用来注入到冒险岛进程才能用的
		*/
		loc = Utility::FindPattern( base, size, ( char * )"8b 47 08 3d 00 ff" ); // fun fact, there is an identical function, dunno what thats about.
		if ( !loc )
		{
			LogB( "Cant find Encryption Function!" );
			return FALSE;
		} // this gets me JUST before they create the encrypted_packet_struct.


		// 将自己编写的函数作为 hook，【扩展】原有的封包函数
		pHookManager->HookFunctionExt( loc, ( uintptr_t )EncryptionHook, 8, false );

		//pHookManager->HookFunctionExt( base + 0x3B0CB1A - 5, ( uintptr_t )checksum_hook, 5 ); // NOT ONE, looks like only three.
		pHookManager->HookFunctionExt( base + ms_defines::world_change_checksum, ( uintptr_t )checksum_hook, ms_defines::world_change_checksum_length ); // 89 0c 24 b9 0 0 0 0 01 d1 02 01, unlikely to last past updates.
		pHookManager->HookFunctionExt( base + ms_defines::world_change_checksum2, ( uintptr_t )checksum_two_hook, ms_defines::world_change_checksum2_length );
		// there is DEFINITELY something in EDX at this hook vv though. Looks like a checksum to me, but apparently not important?
		//pHookManager->HookFunctionExt( base + 0x38732B2 - 5, ( uintptr_t )checksum_two_hook, 5 ); // wait this doesnt work either LOL.
		// well those 2 hooks being commented out didnt crash us on world change. I'll leave them there just in case we want to uncomment it though.

		// I have noticed, every update changes the register used, however in EVERY checksum function, the same register is used. Find it once, find it forever.
		// however, the instructions around it differ.


		/*
			根据特征码查找解包函数
		*/

		// believe packet decryption happens 8d 4d f8 89 43 04 in this function. At this spot, we should be able to get it from either EAX or [ebx+4]
		loc = Utility::FindPattern( base, size, ( char * )"6a 0 6a 4 8b 4d ec" );
		if ( !loc )
		{
			LogB( "Can't find decryption function!" );
			return FALSE;
		}

		// 将自己编写的函数作为 hook，【扩展】原有的解包函数
		pHookManager->HookFunctionExt( loc, ( uintptr_t )DecryptHook, 7 );

		/*
			For anyone who may have the fortune of stumbling upon my code.
			Unsure if these change every update. Just too lazy to sig it at the moment.
		*/
		//pHookManager->HookFunctionExt( base + 0x71280C, ( uintptr_t )EncryptedPacketIDHook, 5, false );
		//pHookManager->HookFunctionExt( 0x0177B991, ( uintptr_t )TestHook );

		LogB( "Everything hooked." );

		// hook 完毕，创建一个线程来执行输入循环
		// 目的是绑定并检测热键，根据按下的热键来开/关抓包功能
		CloseHandle( CreateThread( 0, 0, InputLoop, hThis, 0, 0 ) );
	}

	return TRUE;
}

//void TestHook( Utility::x86Registers * pRegs )
//{
//
//	LogB( "Saw packet id %X", pRegs->edx );
//
//}